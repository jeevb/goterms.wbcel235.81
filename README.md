# Worm and Human Orthologs GO Terms
### Compatible with Ensembl WBcel235 Release 81

##### Usage:
```
# Install package
devtools::install_bitbucket('jeevb/GOTerms.WBcel235.81')

# Load library
library(GOTerms.WBcel235.81)

# Source dataset
data(worm.go)
```

Data was sourced from the `biomaRt` R package as follows:
```
to.list <- function(dat,
                    genes.col='ensembl_gene_id',
                    go.id.cols=c('go_id', 'name_1006', 'namespace_1003')) {
    genes <- dat[, genes.col]
    go.terms <- apply(dat[, go.id.cols], 1, paste, collapse='|')

    plyr::llply(split(genes, go.terms), unique)
}


##
## Main
##

require(biomaRt)
require(dplyr)

worm.db <- useMart('ensembl', dataset='celegans_gene_ensembl')
human.db <- useMart('ensembl', dataset='hsapiens_gene_ensembl')

# Worm GO terms
worm.go <- getBM(
    attributes=c('ensembl_gene_id',
                 'go_id',
                 'name_1006',
                 'go_linkage_type',
                 'namespace_1003'),
    mart=worm.db
    )
worm.go.list <- to.list(worm.go)

# Human GO terms
human.go <- getBM(
    attributes=c('ensembl_gene_id',
                 'go_id',
                 'name_1006',
                 'go_linkage_type',
                 'namespace_1003'),
    mart=human.db
    )

# Worm-Human orthologs
# Note:
# hsapiens_homolog_perc_id is % identity with respect to worm gene
# hsapiens_homolog_perc_id)r1 is % identity with respect to human homolog
human.worm.orthologs <- getBM(
    attributes=c('ensembl_gene_id',
                 'hsapiens_homolog_ensembl_gene',
                 'hsapiens_homolog_orthology_type',
                 'hsapiens_homolog_subtype',
                 'hsapiens_homolog_orthology_confidence',
                 'hsapiens_homolog_perc_id',
                 'hsapiens_homolog_perc_id_r1'),
    mart=worm.db
    )

# Worm-Human orthologous GO terms
human.worm.go <-
    merge(human.worm.orthologs,
          human.go,
          by.x='hsapiens_homolog_ensembl_gene',
          by.y='ensembl_gene_id') %>%
    filter(go_id != '')
human.worm.go.list <- to.list(human.worm.go)
```
